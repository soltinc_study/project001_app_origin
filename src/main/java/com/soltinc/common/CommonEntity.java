package com.soltinc.common;

import java.util.Date;

public interface CommonEntity {
	void setRegistedAt(Date date);

	Date getRegistedAt();

	// 基本非同期で処理が走るので、Userクラスは入れられない。メールアドレスを入れる。
	void setRegistedBy(String user);

	String getRegistedBy();

	void setUpdatedAt(Date date);

	Date getUpdatedAt();

	void setUpdatedBy(String user);

	String getUpdatedBy();
}
