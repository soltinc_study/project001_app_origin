package com.soltinc.common;

public class StringUtils {

	public static String trim(String str) {
		return str.replaceAll("\\r\\n", "").replaceAll("\r\n", "")
				.replaceAll("¥r¥n", "").trim();
	}

	public static String cutTo(String str, String key) {
		return str.substring(str.indexOf(key) + key.length());
	}

	public static String spoitTo(String str, String key) {
		if (str.indexOf(key) == -1) {
			return null;
		}
		return str.substring(0, str.indexOf(key));
	}

}
