package com.soltinc.common;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;

public class DBAccess {
	private final Objectify ofy;

	public DBAccess() {
		ofy = ObjectifyService.ofy();
	}

	public void save(CommonEntity entity, String userId) {
		Date date = new Date();
		save(entity, userId, date);
	}

	public void save(CommonEntity entity, String userId, Date date) {
		if (entity.getRegistedAt() == null) {
			entity.setRegistedAt(date);
			entity.setRegistedBy(userId);
		}
		entity.setUpdatedAt(date);
		entity.setUpdatedBy(userId);
		ofy.save().entity(entity).now();
	}

	public <T> Object get(Class<T> clazz, String id, CommonEntity parent) {
		return ofy.load().type(clazz).parent(parent).id(id).now();
	}

	public <T> List<T> list(Class<T> clazz, Map<String, Object> params) {
		return list(clazz, params, -1, -1);
	}

	public <T> List<T> list(Class<T> clazz, Map<String, Object> params,
			int offset, int limit) {
		return list(clazz, params, offset, limit, null);
	}

	public <T> List<T> list(Class<T> clazz, Map<String, Object> params,
			int offset, int limit, String order) {
		Query<T> type = ofy.load().type(clazz);
		Iterator<String> ite = params.keySet().iterator();
		while (ite.hasNext()) {
			String key = ite.next();
			Object value = params.get(key);
			if (value instanceof List) {
				type = type.filter(key + " in", value);
			} else {
				type = type.filter(key, value);
			}
		}
		if (order != null) {
			type = type.order(order);
		}
		if (limit != -1) {
			type = type.limit(limit);
		}
		if (offset != -1) {
			type = type.offset(offset);
		}
		return type.list();
	}

	public <T> List<T> list(Class<T> clazz, Map<String, Object> params,
			CommonEntity parent) {
		Query<T> type = ofy.load().type(clazz);
		type = type.ancestor(parent);
		Iterator<String> ite = params.keySet().iterator();
		while (ite.hasNext()) {
			String key = ite.next();
			Object value = params.get(key);
			if (value instanceof List) {
				type = type.filter(key + " in", value);
			} else {
				type = type.filter(key, value);
			}
		}
		return type.list();
	}

	public void delete(Class<?> clazz, String id, Object parent) {
		ofy.delete().type(clazz).parent(parent).id(id).now();
	}

	public void delete(Class<?> clazz, String id) {
		ofy.delete().type(clazz).id(id).now();
	}

	public <T> Object get(Class<T> clazz, long id) {
		return ofy.load().type(clazz).id(id).now();
	}

	public <T> Object get(Key<T> key) {
		return ofy.load().key(key).now();
	}

	public <T> Object get(Class<T> clazz, long id, Object parent) {
		return ofy.load().type(clazz).parent(parent).id(id).now();
	}
}
