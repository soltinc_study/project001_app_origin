package com.soltinc.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class NetUtils {
	private static final Log log = LogFactory.getLog(NetUtils.class);

	public static String doGet(String surl) throws IOException {
		URL url = new URL(surl);

		HttpURLConnection connection = null;
		StringBuffer buf = new StringBuffer();

		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");

			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				Map<String, List<String>> map = connection.getHeaderFields();
				for (Map.Entry<String, List<String>> entry : map.entrySet()) {
					log.info("Key : " + entry.getKey() + " ,Value : "
							+ entry.getValue());
				}
				InputStreamReader isr = new InputStreamReader(
						connection.getInputStream(), StandardCharsets.UTF_8);
				BufferedReader reader = new BufferedReader(isr);
				String line;
				while ((line = reader.readLine()) != null) {
					buf.append(line + "¥r¥n");
				}
			}
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		return buf.toString();
	}

	public static byte[] doGetImage(String surl) throws IOException {
		URL url = new URL(surl); // ダウンロードする URL
		URLConnection conn = url.openConnection();
		InputStream in = conn.getInputStream();

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		while (true) {
			int len = in.read(buffer);
			if (len < 0) {
				break;
			}
			out.write(buffer, 0, len);
		}

		out.flush();
		out.close();
		in.close();
		return out.toByteArray();
	}

	

	public static String doPost(String surl, Map<String, Object> params) {
		return doPost(surl, params, null);
	}

	public static String doPost(String surl, Map<String, Object> params,
			String token) {
		try {
			URL url = new URL(surl);

			HttpURLConnection connection = null;

			try {
				connection = (HttpURLConnection) url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestMethod("POST");
				// connection.setRequestProperty("Content-Type",
				// "application/x-www-form-urlencoded; charset=UTF-8");
				connection.setRequestProperty("User-Agent", "curl/7.43.0");
				connection.setRequestProperty("Accept", "*/*");
				connection.setRequestProperty("Content-Type",
						"application/x-www-form-urlencoded");

				// connection.setRequestProperty("Content-Type",
				// "application/json");
				if (token != null) {
					connection.setRequestProperty("Authorization", "Token "
							+ token);
				}
				Iterator<String> ite = params.keySet().iterator();
				boolean isIn = false;
				ByteArrayOutputStream bout = new ByteArrayOutputStream();
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(bout, "UTF8"));
				while (ite.hasNext()) {
					String key = ite.next();
					Object val = params.get(key);
					if (isIn) {
						writer.write("&");
					}
					isIn = true;
					writer.write(key + "=" + val);
				}
				writer.flush();
				connection.setRequestProperty("Content-Length",
						"" + bout.toByteArray().length);

				connection.connect();
				writer = new BufferedWriter(new OutputStreamWriter(
						connection.getOutputStream(), StandardCharsets.UTF_8));
				writer.write(new String(bout.toByteArray(), "UTF8"));
				writer.flush();
				writer.close();

				// if (connection.getResponseCode() ==
				// HttpURLConnection.HTTP_OK) {
				try (InputStreamReader isr = new InputStreamReader(
						connection.getInputStream(), StandardCharsets.UTF_8);
						BufferedReader reader = new BufferedReader(isr)) {
					String line;
					StringBuffer buff = new StringBuffer();
					while ((line = reader.readLine()) != null) {
						buff.append(line + "\r\n");
					}
					return buff.toString();
				}
				// } else {
				// return "But Response:" + connection.getResponseCode() + ":"
				// + params;
				// }
			} finally {
				if (connection != null) {
					connection.disconnect();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			return e.toString();
		}

	}
}
