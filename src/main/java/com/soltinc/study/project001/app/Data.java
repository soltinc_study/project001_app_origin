package com.soltinc.study.project001.app;

import java.util.Date;

import com.soltinc.common.CommonEntity;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * The @Entity tells Objectify about our entity. We also register it in
 * OfyHelper.java -- very important.
 * 
 * This is never actually created, but gives a hint to Objectify about our
 * Ancestor key.
 */
@Entity
public class Data implements CommonEntity {

	@Id
	private Long id;

	@Index
	private String title;

	@Index
	private String category;

	@Index
	private Integer picture;

	private Text description;

	@Index
	private Double price;

	@Index
	private Integer sort;


	private String registedBy;

	private Date registedAt;

	private String updatedBy;

	private Date updatedAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getPicture() {
		return picture;
	}

	public void setPicture(Integer picture) {
		this.picture = picture;
	}

	public String getDescription() {
		return description == null ? null : description.getValue();
	}

	public void setDescription(String description) {
		this.description = description == null ? null : new Text(description);
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getRegistedBy() {
		return registedBy;
	}

	public void setRegistedBy(String registedBy) {
		this.registedBy = registedBy;
	}

	public Date getRegistedAt() {
		return registedAt;
	}

	public void setRegistedAt(Date registedAt) {
		this.registedAt = registedAt;
	}


	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

}
// [END all]
