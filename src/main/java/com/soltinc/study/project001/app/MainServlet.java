package com.soltinc.study.project001.app;

import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.arnx.jsonic.JSON;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.soltinc.common.DBAccess;

public class MainServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2240747556122011556L;

	// Process the http POST of the form
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		String cmd = req.getParameter("cmd");

		DBAccess accessor = new DBAccess();
		if ("loadData".equals(cmd)) {
			//2, データを取得する
			//List<Data> datas = accessor.list(Data.class, new HashMap<String, Object>(), -1, -1, "sort");
			//Map<String, Object> res = new HashMap<String, Object>();
			//res.put("datas", datas);
			//resp.setContentType("application/json;charset=UTF-8");
			//Writer writer = resp.getWriter();
			//writer.write(res);
			//3, データをjsonに変換する
			//writer.write(JSON.encode(res));
			//writer.close();
		}
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser(); // Find out who the user is.
		
		String cmd = req.getParameter("cmd");

		DBAccess accessor = new DBAccess();
		if ("addData".equals(cmd)) {
			//1, API経由で渡されたデータを、DBに挿入する
			//List<Data> datas = accessor.list(Data.class, new HashMap<String, Object>());
			Data data = new Data();
			Date now = new Date();
			data.setTitle(req.getParameter("title"));
			data.setDescription(req.getParameter("description"));
			data.setPrice(Double.parseDouble(req.getParameter("price")));
			data.setCategory(req.getParameter("category"));
			data.setPicture(Integer.parseInt(req.getParameter("picture")));
			//data.setSort(datas.size());
			data.setUpdatedAt(now);
			data.setUpdatedBy(user == null ? null : user.getUserId());
			data.setRegistedAt(now);
			data.setRegistedBy(user == null ? null : user.getUserId());
			//accessor.save(data, user == null ? null : user.getUserId());
		}

	}
}