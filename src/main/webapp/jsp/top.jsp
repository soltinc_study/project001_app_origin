<%-- //[START all]--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>

<%-- //[START imports]--%>
<%@ page import="com.googlecode.objectify.Key" %>
<%@ page import="com.googlecode.objectify.ObjectifyService" %>
<%-- //[END imports]--%>

<%@ page import="java.util.List" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <link type="text/css" rel="stylesheet" href="/stylesheets/main.css"/>
</head>

<body>

<%

    UserService userService = UserServiceFactory.getUserService();
    User user = userService.getCurrentUser();
    if (user != null) {
        pageContext.setAttribute("user", user);
%>

<p style="text-align:right">Hello, ${fn:escapeXml(user.nickname)}! (You can
    <a href="<%= userService.createLogoutURL(request.getRequestURI()) %>">sign out</a>.)</p>
<%
    } else {
%>
<p style="text-align:right">Hello!
    <a href="<%= userService.createLoginURL(request.getRequestURI()) %>">Sign in</a>
    to include your name with greetings you post.</p>
<%
    }
%>
<h2>SOLT study time 2016/06/25 14:00〜18:00</h2>
<br/>
<p>14:00〜14:30&nbsp;&nbsp;setup time</p>
<p>14:30〜15:30&nbsp;&nbsp;[part1] create api at Google Cloud Platform - App Engine -</p>
<p>15:30〜16:00&nbsp;&nbsp;followup time</p>
<p>16:00〜17:00&nbsp;&nbsp;[part2] create android application using GAE </p>
<p>17:00〜17:30&nbsp;&nbsp;followup time</p>
<p>17:30〜18:00&nbsp;&nbsp;relux time</p>
<p>18:00&nbsp;&nbsp;end</p>


</body>
</html>
<%-- //[END all]--%>
